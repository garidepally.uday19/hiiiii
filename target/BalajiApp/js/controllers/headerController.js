samvit.controller("headerController",[
				'$scope',
				'$route',
				'$http',
				'$rootScope',
				'OAuth',
				'$location',
				'OAuthToken',
				function($scope, $route, $http, $rootScope, OAuth,$location,OAuthToken){
					
					//$scope.teacherId = $rootScope.appLoginUser.userId;
					
					$scope.logoutMe = function()	{
						/*var userId = $rootScope.appLoginUser.userId;
					  $http.get(
							'user/logOutMe/' + userId)
							.success(function(response, status, headers) {
								if (response.status == 'success') {
									console.log("LogOut successfully");
									console.log(response.result); 
								} else {
									console.log(response);
								}
							});*/
						OAuthToken.removeToken();
						
						delete $rootScope.appLoginUser;
						
						$scope.$parent.$parent.headerTemplate = "views/header.html";
					      
						$location.path('index');	 
					
				}
				}]);