package com.casoftware.service;

import java.util.List;

import com.casoftware.modal.User;

public interface MainService {

	List<User> getAllUsers();

}
