package com.casoftware.constants;

public class CaSoftwareConstants {
	
	public static final String USER_ROLE = "USER";
	public static final String ADMIN_ROLE = "ADMIN";
	
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";

}