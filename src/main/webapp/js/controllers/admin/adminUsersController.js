samvit.controller("adminUsersController",[
				'$scope',
				'$route',
				'$http',
				'$rootScope',
				'OAuth',
				'$location',
				function($scope, $route, $http, $rootScope, OAuth,$location){
					$scope.headerTemplate = "views/admin/adminHeader.html";
					//fetch all Users Details
					$scope.pageInit = function(){
						$http.get('main/getAllUsers').success(
							function(response, status, headers) {
								if (response.status == 'success') {
									$scope.users = response.result;
								} else {
									$scope.users = null;
								}
						});
					};
					
				}]);